-- MySQL dump 10.13  Distrib 8.0.28, for macos12.0 (arm64)
--
-- Host: 127.0.0.1    Database: eurekaserver
-- ------------------------------------------------------
-- Server version	5.5.5-10.5.8-MariaDB-1:10.5.8+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `solicitudes`
--

DROP TABLE IF EXISTS `solicitudes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `solicitudes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `usuario_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK39q1f7eknic500nwureq0bjlq` (`usuario_id`),
  CONSTRAINT `FK39q1f7eknic500nwureq0bjlq` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitudes`
--

INSERT INTO `solicitudes` VALUES (2,'silviopd descripción','1991-12-31','silviopd',NULL),(3,NULL,'1991-12-31','silviopd',2),(4,'silviopd descripción','1991-12-31','silviopd',3),(5,'silviopd descripción','1991-12-31','silviopd',3),(6,'silviopd descripción','1991-12-31','silviopd',3),(7,'silviopd descripción','1991-12-31','silviopd',3),(8,'silviopd descripción','1991-12-31','silviopd',3),(9,'silviopd descripción','1991-12-31','silviopd',3),(10,'silviopd descripción','1991-12-31','silviopd',3),(11,'silviopd descripción','1991-12-31','silviopd',3),(12,'silviopd descripción','1991-12-31','silviopd',4),(13,'silviopd descripción','1991-12-31','silviopd',4),(14,'silviopd descripción','1991-12-31','silviopd',4),(15,'silviopd descripción','1991-12-31','silviopd',4),(16,'silviopd descripción','1991-12-31','silviopd',4),(17,'silviopd descripción','1991-12-31','silviopd',4),(18,'silviopd descripción','1991-12-31','silviopd',4),(19,'silviopd descripción','1991-12-31','silviopd',4),(20,'silviopd descripción','1991-12-31','silviopd',4),(21,'silviopd descripción','1991-12-31','silviopd',4),(22,'silviopd descripción','1991-12-31','silviopd',4),(23,'silviopd descripción','1991-12-31','silviopd',4),(24,'silviopd descripción','1991-12-31','silviopd',4),(25,'silviopd descripción','1991-12-31','silviopd',4),(26,'silviopd descripción','1991-12-31','silviopd',4),(27,'silviopd descripción','1991-12-31','silviopd',4),(28,'silviopd descripción','1991-12-31','silviopd',4),(29,'silviopd descripción','1991-12-31','silviopd',4),(30,'silviopd descripción','1991-12-31','silviopd',2),(31,'silviopd descripción','1991-12-31','silviopd',6),(32,'silviopd descripción','1991-12-31','silviopd',7),(33,'silviopd descripción','1991-12-31','silviopd',3),(34,'silviopd descripción','1991-12-31','silviopd',4),(35,'silviopd descripción','1991-12-31','silviopd',4),(36,'silviopd descripción','1991-12-31','silviopd',4),(37,'silviopd descripción','1991-12-31','silviopd',4),(38,'silviopd descripción','1991-12-31','silviopd',4),(39,'silviopd descripción','1991-12-31','silviopd',4),(40,'silviopd descripción','1991-12-31','silviopd',4),(41,'silviopd descripción','1991-12-31','silviopd',4),(42,'silviopd descripción','1991-12-31','silviopd',4),(43,'silviopd descripción','1991-12-31','silviopd',4),(44,'silviopd descripción','1991-12-31','silviopd',4),(45,'silviopd descripción','1991-12-31','silviopd',4);

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `apellido` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fecha_nacimiento` datetime DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` VALUES (2,'peña diaz22','silviopd01@gmail.com','1991-12-31 00:00:00','silvio22'),(3,'peña diaz','oswaldo@gmail.com','1991-12-31 00:00:00','oswaldo'),(4,'peña diaz','oswaldo@gmail.com','1991-12-31 00:00:00','oswaldo'),(5,'peña diaz','oswaldo@gmail.com','1991-12-31 00:00:00','oswaldo'),(6,'peña diaz','oswaldo@gmail.com','1991-12-31 00:00:00','oswaldo'),(7,'peña diaz','oswaldo@gmail.com','1991-12-31 00:00:00','oswaldo');
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-05  3:34:43
