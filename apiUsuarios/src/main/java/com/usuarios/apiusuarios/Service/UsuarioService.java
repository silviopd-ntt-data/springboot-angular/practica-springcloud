package com.usuarios.apiusuarios.Service;

import com.usuarios.apiusuarios.Entity.Usuarios;

import java.util.List;

public interface UsuarioService {

    //CRUD
    public List<Usuarios> listar();

    public Usuarios registrar(Usuarios usuarios);

    public Usuarios modificar(Long id, Usuarios usuarios);

    public String eliminar(Long id);

    Usuarios buscarPorId(Long id);


}
