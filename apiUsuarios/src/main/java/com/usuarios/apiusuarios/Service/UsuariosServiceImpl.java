package com.usuarios.apiusuarios.Service;

import com.usuarios.apiusuarios.Entity.Usuarios;
import com.usuarios.apiusuarios.Repository.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuariosServiceImpl implements UsuarioService {

    @Value("${server.port}")
    private int port;

    @Autowired
    private UsuariosRepository usuariosRepository;

    @Override
    public List<Usuarios> listar() {
        return usuariosRepository.findAll();
    }

    @Override
    public Usuarios registrar(Usuarios usuarios) {
        return usuariosRepository.save(usuarios);
    }

    @Override
    public Usuarios modificar(Long id, Usuarios usuarios) {
        usuarios.setId(id);
        return usuariosRepository.save(usuarios);
    }

    @Override
    public String eliminar(Long id) {
        try {
            usuariosRepository.deleteById(id);
            return "Eliminacion Correcta";
        } catch (Exception ex) {
            return "Error durante la eliminacion";
        }
    }

    @Override
    public Usuarios buscarPorId(Long id) {
        Optional<Usuarios> usuarios = usuariosRepository.findById(id);
        if (usuarios.isPresent()) {
            usuarios.get().setPort(port);
            return usuarios.get();
        }
        return null;
    }
}
