package com.usuarios.apiusuarios.Controller;

import com.usuarios.apiusuarios.Entity.Usuarios;
import com.usuarios.apiusuarios.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UsuariosController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping("/listar")
    List<Usuarios> listar() {
        return usuarioService.listar();
    }

    @PostMapping("/crear")
    Usuarios crear(@RequestBody Usuarios usuario) {
        return usuarioService.registrar(usuario);
    }

    @PutMapping("/modificar/{id}")
    Usuarios modificar(@RequestHeader HttpHeaders headers, @PathVariable("id") Long id, @RequestBody Usuarios usuario) {
        System.out.println("CABECERAS -->" + headers.toString());
        return usuarioService.modificar(id, usuario);
    }

    @DeleteMapping("/eliminar/{id}")
    String eliminar(@PathVariable("id") Long id) {
        return usuarioService.eliminar(id);
    }

    @GetMapping("/buscar/{id}")
    Usuarios buscarPorId(@PathVariable("id") Long id) throws IllegalAccessException {
        if (usuarioService.buscarPorId(id) == null) {
            throw new IllegalAccessException("No existe el usuario con id: " + id);
        } else {
            return usuarioService.buscarPorId(id);
        }
    }
}
