package com.usuarios.apiusuarios.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "solicitudes")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Solicitudes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String titulo;

    @Temporal(TemporalType.DATE)
    private Date fecha;

    private String descripcion;

    @ManyToOne
    @JoinColumn(name = "usuario_id")
    @JsonIgnoreProperties("solicitudesList")
    private Usuarios usuarios;
    
}

