package com.solicitudes.apisolicitudes.Repository;

import com.solicitudes.apisolicitudes.Entity.Solicitudes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SolicitudesRepository extends JpaRepository<Solicitudes, Long> {
}
