package com.solicitudes.apisolicitudes.Controller;

import com.solicitudes.apisolicitudes.Entity.Solicitudes;
import com.solicitudes.apisolicitudes.Service.SolicitudesServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SolicitudesController {

    @Autowired
    private SolicitudesServices solicitudesServices;

    @GetMapping("/listar")
    List<Solicitudes> listar() {
        return solicitudesServices.listar();
    }

    @PostMapping("/crear")
    Solicitudes crear(@RequestBody Solicitudes solicitudes) {
        return solicitudesServices.registrar(solicitudes);
    }

    @PutMapping("/modificar/{id}")
    Solicitudes modificar(@RequestHeader HttpHeaders headers, @PathVariable("id") Long id, @RequestBody Solicitudes solicitudes) {
        System.out.println("CABECERAS -->"+headers.toString());
        return solicitudesServices.modificar(id, solicitudes);
    }

    @DeleteMapping("/eliminar/{id}")
    String eliminar(@PathVariable("id") Long id) {
        return solicitudesServices.eliminar(id);
    }

    @GetMapping("/buscar/{id}")
    Solicitudes buscarPorId(@PathVariable("id") Long id) {
        return solicitudesServices.buscarPorId(id);
    }
}
