package com.solicitudes.apisolicitudes.Feign;

import com.solicitudes.apisolicitudes.Entity.Usuarios;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "usuarios-service")
public interface UsuariosFeignClient {

    @GetMapping("/buscar/{id}")
    Usuarios buscarPorId(@PathVariable("id") Long id);
}
