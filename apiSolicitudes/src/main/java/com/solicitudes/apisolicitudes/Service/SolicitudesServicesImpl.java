package com.solicitudes.apisolicitudes.Service;

import com.solicitudes.apisolicitudes.Entity.Solicitudes;
import com.solicitudes.apisolicitudes.Entity.Usuarios;
import com.solicitudes.apisolicitudes.Feign.UsuariosFeignClient;
import com.solicitudes.apisolicitudes.Repository.SolicitudesRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class SolicitudesServicesImpl implements SolicitudesServices {

    @Autowired
    private SolicitudesRepository solicitudesRepository;

    @Autowired
    private UsuariosFeignClient usuariosFeignClient;

    @Override
    public List<Solicitudes> listar() {
        return solicitudesRepository.findAll();
    }

    @Override
    @CircuitBreaker(name = "usuariosbuscar", fallbackMethod = "metodoAlternativo")
    public Solicitudes registrar(Solicitudes solicitudes) {
        Usuarios usuarios = usuariosFeignClient.buscarPorId(solicitudes.getUsuarios().getId());
        System.out.println(usuarios);
        if (Objects.nonNull(usuarios)) {
            Solicitudes solicitud = solicitudesRepository.save(solicitudes);
            solicitud.setUsuarios(usuarios);
            return solicitud;
        }
        return null;
    }

    private Solicitudes metodoAlternativo(Throwable e) {
        System.out.println(e.getMessage());

        Solicitudes solicitudes = new Solicitudes();
        solicitudes.setId(0L);
        solicitudes.setTitulo("Error");
        solicitudes.setDescripcion("Error");
        return solicitudes;
    }

    @Override
    public Solicitudes modificar(Long id, Solicitudes solicitudes) {
        solicitudes.setId(id);
        return solicitudesRepository.save(solicitudes);
    }

    @Override
    public String eliminar(Long id) {
        try {
            solicitudesRepository.deleteById(id);
            return "Eliminacion Correcta";
        } catch (Exception ex) {
            return "Error durante la eliminacion";
        }
    }

    @Override
    public Solicitudes buscarPorId(Long id) {
        Optional<Solicitudes> solicitudes = solicitudesRepository.findById(id);
        if (solicitudes.isPresent()) {
            return solicitudes.get();
        }
        return null;
    }
}
