package com.solicitudes.apisolicitudes.Service;

import com.solicitudes.apisolicitudes.Entity.Solicitudes;

import java.util.List;

public interface SolicitudesServices {

    //CRUD
    public List<Solicitudes> listar();

    public Solicitudes registrar(Solicitudes solicitudes);

    public Solicitudes modificar(Long id, Solicitudes solicitudes);

    public String eliminar(Long id);

    Solicitudes buscarPorId(Long id);

}
